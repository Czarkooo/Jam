using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{
    private static GameObject _instance;

    private void Awake()
    {
        if (_instance)
        {
            DestroyImmediate(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        _instance = gameObject;
    }
}
