using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Asteroid : MonoBehaviour
{
	[SerializeField] private float _minScale;
	[SerializeField] private float _maxScale;
	[SerializeField] private float _minSize;
	[SerializeField] private float _maxSize;
	[SerializeField] private float _minSpeed;
	[SerializeField] private float _maxSpeed;
	[SerializeField] private float _rotationTorque;
	[SerializeField] private float _damage;
	private Rigidbody _rigidbody;

	private void Awake()
	{
		_rigidbody = GetComponent<Rigidbody>();
	}

	private void Start()
	{
		_rigidbody.velocity = Random.onUnitSphere * Random.Range(_minSpeed, _maxSpeed);

		transform.localScale = new Vector3(
			Random.Range(_minScale, _maxScale),
			Random.Range(_minScale, _maxScale),
			Random.Range(_minScale, _maxScale)
		) * Random.Range(_minSize, _maxSize);
		
		_rigidbody.AddTorque(new Vector3(
			Random.Range(-_rotationTorque, _rotationTorque),
			Random.Range(-_rotationTorque, _rotationTorque),
			Random.Range(-_rotationTorque, _rotationTorque)
		));
	}

	private void OnCollisionEnter(Collision other)
	{
		HealthSystem _healthSystem = other.gameObject.GetComponent<HealthSystem>();
		if (_healthSystem != null)
		{
			_healthSystem.Damage(_damage);
		}
	}
}
