using UnityEngine;

[RequireComponent(typeof(HealthSystem))]
public class PlayerController : MonoBehaviour
{
    private HealthSystem _healthSystem;
    [SerializeField] private EndPanel _endPanel;
    [SerializeField] private GameObject _particle;

    private void Awake()
    {
        _healthSystem = GetComponent<HealthSystem>();
        _healthSystem.OnDeath.AddListener(OnDeath);
    }

    private void OnDeath()
    {
        _particle.transform.position = transform.position;
        _particle.SetActive(true);
        Destroy(gameObject);
        _endPanel.End();
    }
}
