using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private bool _isMute;
    [SerializeField] private Button _playButton;
    [SerializeField] private Button _quitButton;
    [SerializeField] private Button _soundButton;
    [SerializeField] private Sprite _soundOn;
    [SerializeField] private Sprite _soundOff;
    [SerializeField] private Image _fade;
    
    private float _timer;

    private void Awake()
    {
        _playButton.onClick.AddListener(PlayGame);
        _quitButton.onClick.AddListener(QuitGame);
        _soundButton.onClick.AddListener(Mute);
    }

    private void PlayGame()
    {
        StartCoroutine(Countdown());
    }

    private IEnumerator Countdown()
    {
        _fade.enabled = true;
        while(_timer < 1f)
        {
            _fade.color = new Color(0f, 0f, 0f, _timer);
            _timer += Time.deltaTime;
            yield return null;
        }
        SceneManager.LoadScene(1);
    }

    private void QuitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }

    private void Mute()
    {
        _isMute = !_isMute;
        AudioListener.volume = _isMute ? 0 : 1;
        _soundButton.image.sprite = _isMute ? _soundOff : _soundOn;
    }
}

