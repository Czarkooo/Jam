using UnityEngine;
using UnityEngine.Events;

public class HealthSystem : MonoBehaviour
{
    [SerializeField] private float _health;

    public UnityEvent OnDeath;
    
    public void Damage(float damage)
    {
        _health -= damage;
        if (_health <= 0)
        {
            OnDeath.Invoke();
        }
    }
}
