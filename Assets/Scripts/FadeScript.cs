using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FadeScript : MonoBehaviour
{
    [SerializeField] private Image _fade;
    
    private float _timer;
    private void Awake()
    {
        StartCoroutine(Countdown());
    }
    private IEnumerator Countdown()
    {
        _fade.enabled = true;
        while (_timer < 2f)
        {
            _fade.color = new Color(0f, 0f, 0f, 1 - _timer/2);
            _timer += Time.deltaTime;
            yield return null;
        }
        _fade.enabled = false;
    }
}
