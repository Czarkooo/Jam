using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	[SerializeField] private Transform _target;
	[SerializeField] private Vector3 _defaultDistance;
	[SerializeField] private float _distanceDamp;
	private Vector3 _velocity = Vector3.one;

	private void FixedUpdate()
	{
		if (!_target)
		{
			return;
		}
		
		Vector3 toPosition = _target.position + _target.rotation * _defaultDistance;
		Vector3 currentPosition = Vector3.SmoothDamp(transform.position, toPosition, ref _velocity, _distanceDamp);
		transform.position = currentPosition;
		transform.LookAt(_target, _target.up);
	}
}
