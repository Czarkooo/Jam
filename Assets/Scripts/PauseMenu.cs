using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private bool _gameIsPaused;
    [SerializeField] private bool _isMute;
    [SerializeField] private AudioSource _alarm;
    [SerializeField] private AudioSource _rocets;
    [SerializeField] private GameObject _pausePanel;
    [SerializeField] private Button _resumeButton;
    [SerializeField] private Button _menuButton;
    [SerializeField] private Button _quitButton;
    [SerializeField] private Button _soundButton;
    [SerializeField] private Sprite _soundOn;
    [SerializeField] private Sprite _soundOff;

    private void Start()
    {
        _resumeButton.onClick.AddListener(Resume);
        _menuButton.onClick.AddListener(Menu);
        _quitButton.onClick.AddListener(QuitGame);
        _soundButton.onClick.AddListener(Mute);
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    private void Update()
    {
        if (!Input.GetButtonDown("Cancel"))
        {
            return;
        }
        
        if (_gameIsPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }
    
    private void Mute()
    {
        _isMute = !_isMute;
        AudioListener.volume = _isMute ? 0 : 1;
        _soundButton.image.sprite = _isMute ? _soundOff : _soundOn;
    }

    private void QuitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }

    private void Menu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    
    private void Resume()
    {
        _pausePanel.SetActive(false);
        Time.timeScale = 1f;
        _gameIsPaused = false;
        _rocets.UnPause();
        _alarm.UnPause();
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    private void Pause()
    {
        _pausePanel.SetActive(true);
        Time.timeScale = 0f;
        _gameIsPaused = true;
        _rocets.Pause();
        _alarm.Pause();
        Cursor.lockState = CursorLockMode.None;
    }
}
