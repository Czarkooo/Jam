using System.Collections;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class AsteroidManager : MonoBehaviour
{
    [SerializeField] private GameObject[] _asteroidPrefab;
    [SerializeField] private int _amoutOfAsteroids;
    private SphereCollider _collider;

    private void Awake()
    {
        _collider = GetComponent<SphereCollider>();
    }

    private void Start()
    {
        StartCoroutine(GenerateAsteroids());
    }

    private IEnumerator GenerateAsteroids()
    {
        yield return new WaitForSeconds(8);
        for (int i = 0; i < _amoutOfAsteroids; i++)
        {
            Instantiate(_asteroidPrefab[i % _asteroidPrefab.Length], Random.onUnitSphere * _collider.radius + transform.position, Quaternion.identity);
            if (i % 10 == 0 && i != 0)
            {
                yield return new WaitForSeconds(5);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        other.transform.position = Random.onUnitSphere * _collider.radius + transform.position;
    }
}
