using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AlarmScript : MonoBehaviour
{
    [SerializeField] private GameObject _text;
    private AudioSource _alarm;
    
    private void Awake()
    {
        _alarm = GetComponent<AudioSource>();
        _alarm.Play();
        StartCoroutine(Alarm());
    }
    
    private IEnumerator Alarm()
    {
        for (int i = 0; i < 6; i++)
        {
            _text.SetActive(true);
            yield return new WaitForSeconds(1f);
            _text.SetActive(false);
            yield return new WaitForSeconds(1f);
        }

    }
}
