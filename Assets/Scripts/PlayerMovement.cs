using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
	[SerializeField] private float _speed;
	[SerializeField] private float _boostSpeed;
	[SerializeField] private float _rotateForce;
	[SerializeField] [Range(-1, 1)] private float _yawSensivity;
	[SerializeField] [Range(-1, 1)] private float _pitchSensivity;
	[SerializeField] [Range(-1, 1)] private float _rollSensivity;
	private Rigidbody _rigidbody;
	private float _thrust;
	private float _yaw;
	private float _pitch;
	private float _roll;

	private void Awake()
	{
		_rigidbody = GetComponent<Rigidbody>();
	}

	private void Update()
	{
		_thrust = Input.GetAxis("Vertical");
		_yaw = Input.GetAxis("Mouse X") * _yawSensivity;;
		_pitch = Input.GetAxis("Mouse Y") * _pitchSensivity;
		_roll = Input.GetAxis("Horizontal") * _rollSensivity;
	}

	private void FixedUpdate()
	{
		_rigidbody.velocity = transform.forward * (_speed + _boostSpeed * Mathf.Clamp01(_thrust));

		_rigidbody.AddRelativeTorque(new Vector3(_pitch, _yaw,_roll) * _rotateForce);
	}
}
