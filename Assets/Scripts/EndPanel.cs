using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndPanel : MonoBehaviour
{
    [SerializeField] private bool _isMute;
    [SerializeField] private AudioSource _alarm;
    [SerializeField] private GameObject _pausePanel;
    [SerializeField] private Button _menuButton;
    [SerializeField] private Button _quitButton;
    [SerializeField] private Button _soundButton;
    [SerializeField] private Sprite _soundOn;
    [SerializeField] private Sprite _soundOff;

    private void Start()
    {
        _menuButton.onClick.AddListener(Menu);
        _quitButton.onClick.AddListener(QuitGame);
        _soundButton.onClick.AddListener(Mute);
    }
    
    private void Mute()
    {
        _isMute = !_isMute;
        AudioListener.volume = _isMute ? 0 : 1;
        _soundButton.image.sprite = _isMute ? _soundOff : _soundOn;
    }

    private void QuitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }

    private void Menu()
    {
        SceneManager.LoadScene(0);
    }
    
    public void End()
    {
        _pausePanel.SetActive(true);
        _alarm.Pause();
        Cursor.lockState = CursorLockMode.None;
    }
}
