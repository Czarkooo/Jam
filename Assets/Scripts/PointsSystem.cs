using System.Collections;
using UnityEngine;
using TMPro;

public class PointsSystem : MonoBehaviour
{
    [SerializeField] private float scoreAmount;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private HealthSystem _playerController;
    private bool _isCounting = true;
    
    private void Start()
    {
        scoreAmount = 0f;
        _playerController.OnDeath.AddListener(StopScore);
        StartCoroutine(Countdown());
    }

    private void StopScore()
    {
        _isCounting = false;
    }

    private IEnumerator Countdown()
    {
        while (_isCounting)
        {
            scoreText.text = "Score: " + scoreAmount;
            scoreAmount += 1;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
